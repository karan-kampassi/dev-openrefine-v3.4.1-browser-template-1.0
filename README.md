# openrefine-v3.4.1-browser-template-1.0

This project deploys open refine version 3.4.1 as a browser template.
OpenRefine (previously Google Refine) is a powerful tool for working with messy data: cleaning it; transforming it from one format into another.